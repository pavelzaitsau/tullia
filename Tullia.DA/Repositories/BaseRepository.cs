﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using Dapper;
using PZ.Tullia.DA.Shared;

namespace PZ.Tullia.DA.Repositories
{
    public abstract class BaseRepository
    {
        private static readonly object InitializingLocker = new object();

        private static bool _initialized;

        private readonly string _connectionString;

        protected BaseRepository(IDatabaseConfiguration configuration)
        {
            var connectionString = InitializeAndGetConnectionStaring(configuration);
            _connectionString = connectionString;
        }

        private static string InitializeAndGetConnectionStaring(IDatabaseConfiguration configuration)
        {
            var builder = new SQLiteConnectionStringBuilder(configuration.ConnectionString);

            if (!_initialized)
            {
                lock (InitializingLocker)
                {
                    if (!_initialized)
                    {
                        if (!File.Exists(builder.DataSource)
                            || File.GetLastWriteTimeUtc(builder.DataSource)
                                < File.GetLastWriteTimeUtc(configuration.SeedDatabase))
                        {
                            File.Copy(configuration.SeedDatabase, builder.DataSource, true);
                        }

                        _initialized = true;
                    }
                }
            }

            var connectionString = builder.ConnectionString;

            return connectionString;
        }


        // ReSharper disable once UnusedMethodReturnValue.Global
        protected int Execute(string query, object parameters = null)
        {
            using (var connection = new SQLiteConnection(_connectionString))
            {
                connection.Open();

                return connection.Execute(query, parameters);
            }
        }

        protected IEnumerable<TDataModel> Execute<TDataModel>(string query, object parameters = null)
        {
            using (var connection = new SQLiteConnection(_connectionString))
            {
                connection.Open();

                return connection.Query<TDataModel>(query, parameters);
            }
        }
    }
}
