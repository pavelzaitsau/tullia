﻿using System.Collections.Generic;
using System.Linq;
using PZ.Tullia.DA.Shared;
using PZ.Tullia.DA.Shared.Models;
using PZ.Tullia.DA.Shared.Repositories;

namespace PZ.Tullia.DA.Repositories
{
    public class MunicipalityRepository : BaseRepository, IMunicipalityRepository
    {
        public MunicipalityRepository(IDatabaseConfiguration databaseConfiguration)
            : base(databaseConfiguration)
        {
        }

        public MunicipalityDataModel GetMunicipality(string municipalityName)
        {
            return
                Execute<MunicipalityDataModel>(
                    "SELECT ID, Name FROM Municipalities WHERE Name = @name LIMIT 1",
                    new { name = municipalityName })
                .FirstOrDefault();
        }

        public IEnumerable<MunicipalityDataModel> GetMunicipalities()
        {
            return Execute<MunicipalityDataModel>("SELECT ID, Name FROM Municipalities");
        }

        public void AddMunicipality(string municipalityName)
        {
            Execute("INSERT INTO Municipalities (Name) VALUES (@municipalityName)", new { municipalityName });
        }
    }
}
