﻿using System;
using System.Collections.Generic;
using System.Linq;
using PZ.Tullia.DA.Shared;
using PZ.Tullia.DA.Shared.Models;
using PZ.Tullia.DA.Shared.Repositories;

namespace PZ.Tullia.DA.Repositories
{
    public class TaxRepository : BaseRepository, ITaxRepository
    {
        public TaxRepository(IDatabaseConfiguration databaseConfiguration)
            : base(databaseConfiguration)
        {
        }

        public IEnumerable<TaxDataModel> GetTaxes(int municipalityId)
        {
            return
                Execute<TaxDataModel>(
                    "SELECT ID, MunicipalityID, Priority, StartDate, EndDate, Value FROM Taxes " +
                    "WHERE MunicipalityID = @municipalityId",
                    new { municipalityId });
        }

        public IEnumerable<TaxDataModel> GetTaxesForInterval(int municipalityId, int priority, DateTime startDate, DateTime endDate)
        {
            return
                Execute<TaxDataModel>(
                    "SELECT ID, MunicipalityID, Priority, StartDate, EndDate, Value FROM Taxes " +
                    "WHERE MunicipalityID = @municipalityId " +
                    "AND Priority = @priority " +
                    "AND ((StartDate <= @startDate AND @startDate <= EndDate) " +
                    "   OR (StartDate <= @endDate AND @endDate <= EndDate)) ",
                    new { municipalityId, priority, startDate, endDate });
        }

        public IEnumerable<TaxDataModel> GetTaxesForDate(int municipalityId, DateTime date)
        {
            return
                Execute<TaxDataModel>(
                    "SELECT ID, MunicipalityID, Priority, StartDate, EndDate, Value FROM Taxes " +
                    "WHERE MunicipalityID = @municipalityId " +
                    "AND (StartDate <= @date AND @date <= EndDate) ",
                    new { municipalityId, date });
        }

        public TaxDataModel GetTax(int municipalityId, int priority, DateTime startDate, DateTime endDate)
        {
            return
                Execute<TaxDataModel>(
                    "SELECT ID, MunicipalityID, Priority, StartDate, EndDate, Value FROM Taxes " +
                    "WHERE MunicipalityID = @municipalityId " +
                    "AND Priority = @priority " +
                    "AND (@startDate = StartDate)" +
                    "AND (EndDate = @endDate)) " +
                    "LIMIT 1 ",
                    new { municipalityId, priority, startDate, endDate })
                .FirstOrDefault();
        }

        public void AddTax(int municipalityId, int priority, DateTime startDate, DateTime endDate, double value)
        {
            Execute(
                "INSERT INTO Taxes (MunicipalityID, Priority, StartDate, EndDate, Value) VALUES " +
                "(@municipalityId, @priority, @startDate, @endDate, @value) ",
                new { municipalityId, priority, startDate, endDate, value });
        }

        public void DeleteTax(int taxId)
        {
            Execute(
                "DELETE FROM Taxes WHERE ID = @taxId ",
                new { taxId });
        }
    }
}
