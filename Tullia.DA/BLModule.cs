﻿using Autofac;
using PZ.Tullia.DA.Repositories;
using PZ.Tullia.DA.Shared.Repositories;

namespace PZ.Tullia.DA
{
    public class DAModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MunicipalityRepository>().As<IMunicipalityRepository>();
            builder.RegisterType<TaxRepository>().As<ITaxRepository>();
        }
    }
}
