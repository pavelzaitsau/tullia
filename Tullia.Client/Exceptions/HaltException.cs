﻿using System;

namespace PZ.Tullia.Client.Exceptions
{
    public class HaltException : Exception
    {
        public HaltException()
            : base(string.Empty)
        {
        }

        public HaltException(string message)
            : base(message)
        {
        }
    }
}
