﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("PZ.Tullia.Client")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Tullia")]
[assembly: AssemblyCopyright("Copyright © Pavel Zaitsau 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("2ed07afe-4e10-4784-8ea1-2422c05c4c7d")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
