﻿using System;
using System.IO;
using System.Linq;
using PZ.Tullia.Client.Commands;
using PZ.Tullia.Client.Exceptions;

namespace PZ.Tullia.Client
{
    public static class Program
    {
        public static void Main(string[] args)
        {
#if DEBUG
            Console.WriteLine(string.Join(" ", args ?? new string[0]));
#endif

            var handlers =
                typeof(Program)
                .Assembly
                .GetTypes()
                .Where(x => x.IsClass && x.GetInterface(nameof(ICommandHandler)) != null)
                .Select(x => (ICommandHandler)Activator.CreateInstance(x))
                .ToDictionary(x => x.Key, x => x);

            try
            {
                if (args == null
                    || args.Length < 1
                    || !handlers.TryGetValue(args[0].ToLower(), out var handler))
                {
                    var commands = handlers.Select(x => "[" + x.Key + "]");
                    var assemblyName = Path.GetFileNameWithoutExtension(typeof(Program).Assembly.Location);

                    Console.WriteLine("Unknown command");
                    Console.WriteLine($"Commands {string.Join(", ", commands)} are supported.");
                    Console.WriteLine($"Enter '{assemblyName} [command] help' to get a command help");
                }
                else if (args.Length > 1 && string.Equals(args[1], "help", StringComparison.InvariantCultureIgnoreCase))
                {
                    handler.PrintHelp();
                }
                else
                {
                    handler.Invoke(args.Skip(1).ToArray());
                }
            }
            catch (HaltException e)
            {
                if (!string.IsNullOrWhiteSpace(e.Message))
                {
                    Console.WriteLine(e.Message);
                    Environment.Exit(2);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error has occurred");
                Console.WriteLine(e.Message);

#if DEBUG
                Console.WriteLine(e.StackTrace);
#endif
                Environment.Exit(1);
            }
        }
    }
}
