﻿using System;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace PZ.Tullia.Client
{
    [Serializable]
    public class ErrorModel
    {
        public ErrorModel()
        {
            Errors = new FieldErrorModel[0];
        }

        [UsedImplicitly]
        [JsonProperty("__message")]
        public string Message { get; set; }

        [UsedImplicitly]
        [JsonProperty("__errors")]
        public FieldErrorModel[] Errors { get; set; }

        [Serializable]
        public class FieldErrorModel
        {
            [UsedImplicitly]
            [JsonProperty("__field")]
            public string Field { get; set; }

            [UsedImplicitly]
            [JsonProperty("__message")]
            public string Message { get; set; }
        }
    }
}
