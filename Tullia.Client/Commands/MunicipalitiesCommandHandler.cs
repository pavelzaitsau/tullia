﻿using System;
using System.IO;

namespace PZ.Tullia.Client.Commands
{
    public class MunicipalitiesCommandHandler : BaseCommandHandler, ICommandHandler
    {
        public string Key => "municipality";

        public void Invoke(params string[] args)
        {
            var command = GetArgOrHelp<string>(args, 0).ToLower();
            var uri = GetApiUri(args);

            switch (command)
            {
                case "all":
                {
                    PrintAllMunicipalities(uri);
                    return;
                }
                case "add":
                {
                    var municipalityName = GetArgOrHelp<string>(args, 1);
                    AddMunicipality(uri, municipalityName);
                    return;
                }
                case "import":
                {
                    var path = GetArgOrDefault<string>(args, 1);
                    ImportMunicipalities(uri, path);
                    return;
                }
                default:
                {
                    Console.WriteLine("Wrong arguments");
                    PrintHelp();
                    return;
                }
            }
        }

        private void ImportMunicipalities(Uri uri, string path)
        {
            var municipalities = File.ReadAllLines(path);
            foreach (var municipality in municipalities)
            {
                try
                {
                    AddMunicipality(uri, municipality);
                    Console.WriteLine($"{municipality}: OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{municipality}: FAIL ({ex.Message})");
                }
            }
        }

        private void AddMunicipality(Uri uri, string municipalityName)
        {
            PostRequest<string>(uri, "api/municipality", null, new { municipalityName });
        }

        private void PrintAllMunicipalities(Uri uri)
        {
            var municipalities = GetRequest<string[]>(uri, "api/municipality");

            foreach (var municipality in municipalities)
            {
                Console.WriteLine(municipality);
            }
        }

        public override void PrintHelp()
        {
            Console.WriteLine("all [API uri] - to get all municipalities");
            Console.WriteLine("add [name] [API uri] - to add a municipality");
            Console.WriteLine("import [path] [API uri] - to import municipalities from txt or csv file (NOTE: file should contains one column with out a header)");
        }
    }
}
