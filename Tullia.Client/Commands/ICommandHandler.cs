﻿namespace PZ.Tullia.Client.Commands
{
    public interface ICommandHandler
    {
        string Key { get; }

        void Invoke(params string[] args);

        void PrintHelp();
    }
}
