﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using PZ.Tullia.Client.Exceptions;
using RestSharp;

namespace PZ.Tullia.Client.Commands
{
    public abstract class BaseCommandHandler
    {
        protected T GetArgOrHelp<T>(IEnumerable<string> args, int index, IFormatProvider provider = null)
        {
            var arg = args.ElementAtOrDefault(index);
            if (arg == null)
            {
                PrintHelp();
                throw new HaltException();
            }

            return (T)Convert.ChangeType(arg, typeof(T), provider);
        }

        protected static T GetArgOrDefault<T>(IEnumerable<string> args, int index, IFormatProvider provider = null)
        {
            var arg = args.ElementAtOrDefault(index);
            if (arg == null)
            {
                return default;
            }

            try
            {
                return (T)Convert.ChangeType(arg, typeof(T), provider);
            }
            catch (FormatException)
            {
                return default;
            }
        }

        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
        protected Uri GetApiUri(string[] args, int minPosition = 0)
        {
            var uri = args.LastOrDefault();
            if (uri == null || args.Length - 1 < minPosition)
            {
                PrintHelp();
                throw new HaltException();
            }

            if (uri.StartsWith("http://") || uri.StartsWith("https://"))
            {
                return new Uri(uri);
            }

            uri = "http://" + uri;
            return new Uri(uri);
        }
        protected static TModel GetRequest<TModel>(Uri uri, string resource, object urlParams = null)
            where TModel : class
        {
            return Request<TModel>(uri, Method.GET, resource, null, urlParams);
        }

        protected static TModel PutRequest<TModel>(Uri uri, string resource, object body = null, object urlParams = null)
            where TModel : class
        {
            return Request<TModel>(uri, Method.PUT, resource, body, urlParams);
        }

        protected static TModel PostRequest<TModel>(Uri uri, string resource, object body = null, object urlParams = null)
            where TModel : class
        {
            return Request<TModel>(uri, Method.POST, resource, body, urlParams);
        }

        protected static TModel DeleteRequest<TModel>(Uri uri, string resource, object body = null, object urlParams = null)
            where TModel : class
        {
            return Request<TModel>(uri, Method.DELETE, resource, body, urlParams);
        }

        private static TModel Request<TModel>(Uri uri, Method method, string resource, object body, object urlParams)
            where TModel : class
        {
            var restClient = new RestClient(uri);
            var request = new RestRequest
            {
                RequestFormat = DataFormat.Json,
                Method = method
            };

            request.AddHeader("Accept", "application/json");

            request.Resource = resource;

            if (urlParams != null)
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(urlParams));
                foreach (var urlParam in dict)
                {
                    request.AddQueryParameter(urlParam.Key, urlParam.Value);
                }
            }

            if (body != null)
            {
                request.AddJsonBody(body);
            }

            var response = restClient.Execute(request);

            TModel model = null;
            ErrorModel error = null;

            try
            {
                model = JsonConvert.DeserializeObject<TModel>(response.Content);
            }
            catch (JsonSerializationException)
            {
            }
            catch (JsonReaderException)
            {
            }

            try
            {
                error = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
            }
            catch (JsonSerializationException)
            {
            }
            catch (JsonReaderException)
            {
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var message = error?.Message;
                if (string.IsNullOrWhiteSpace(message))
                {
                    message = $"Server error has occured (CODE: {response.StatusCode})";
                }

                if (error?.Errors?.Length > 0)
                {
                    message = string.Concat(
                        message,
                        Environment.NewLine,
                        string.Join(Environment.NewLine, error.Errors.Select(x => x.Field + " - " + x.Message)));
                }

                throw new HaltException(message);
            }

            return model;
        }

        public abstract void PrintHelp();
    }
}
