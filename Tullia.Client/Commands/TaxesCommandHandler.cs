﻿using System;
using System.Globalization;
using System.Linq;
using PZ.Tullia.BL.Shared.Models;
using PZ.Tullia.Client.Exceptions;

namespace PZ.Tullia.Client.Commands
{
    public class TaxesCommandHandler : BaseCommandHandler, ICommandHandler
    {
        public string Key => "tax";

        public void Invoke(params string[] args)
        {
            var command = GetArgOrHelp<string>(args, 0).ToLower();
            var uri = GetApiUri(args, 2);

            switch (command)
            {
                case "get":
                {
                    var municipalityName = GetArgOrHelp<string>(args, 1);
                    var year = GetArgOrDefault<int>(args, 2);
                    var month = GetArgOrDefault<int>(args, 3);
                    var day = GetArgOrDefault<int>(args, 4);

                    if (year != 0)
                    {
                        PrintDayTax(uri, municipalityName, year, month, day);
                    }
                    else
                    {
                        PrintAllTaxes(uri, municipalityName);
                    }

                    return;
                }
                case "add":
                {
                    var taxType = GetArgOrDefault<string>(args, 1);
                    var municipalityName = GetArgOrHelp<string>(args, 2);

                    switch (taxType)
                    {
                        case "yearly":
                        {
                            var year = GetArgOrHelp<int>(args, 3);
                            var tax = GetArgOrHelp<double>(args, 4);

                            AddYearlyTax(uri, municipalityName, year, tax);

                            break;
                        }
                        case "monthly":
                        {
                            var year = GetArgOrHelp<int>(args, 3);
                            var month = GetArgOrHelp<int>(args, 4);
                            var tax = GetArgOrHelp<double>(args, 5);

                            AddMonthlyTax(uri, municipalityName, year, month, tax);

                            break;
                        }
                        case "weekly":
                        {
                            var year = GetArgOrHelp<int>(args, 3);
                            var weekNumber = GetArgOrHelp<int>(args, 4);
                            var tax = GetArgOrHelp<double>(args, 5);

                            AddWeeklyTax(uri, municipalityName, year, weekNumber, tax);

                            break;
                        }
                        case "daily":
                        {
                            var year = GetArgOrHelp<int>(args, 3);
                            var month = GetArgOrDefault<int>(args, 4);
                            var day = GetArgOrDefault<int>(args, 5);
                            var tax = GetArgOrHelp<double>(args, 6);

                            AddDailyTax(uri, municipalityName, year, month, day, tax);

                            break;
                        }
                        default:
                        {
                            PrintHelp();
                            break;
                        }
                    }

                    return;
                }
                default:
                {
                    Console.WriteLine("Wrong arguments");
                    PrintHelp();
                    return;
                }
            }
        }

        public override void PrintHelp()
        {
            Console.WriteLine("get [municipality name] [year] [month] [day] [API uri]                - get tax for the day");
            Console.WriteLine("get [municipality name]                                               - get all for municipality");
            Console.WriteLine("add yearly [municipality name] [year] [value] [API uri]               - add yearly tax");
            Console.WriteLine("add monthly [municipality name] [year] [month] [value] [API uri]      - add monthly tax");
            Console.WriteLine("add weekly [municipality name] [year] [week number] [value] [API uri] - add weekly tax");
            Console.WriteLine("add daily [municipality name] [year] [month] [day] [value] [API uri]  - add weekly tax");
        }

        private void AddDailyTax(Uri uri, string municipalityName, int year, int month, int day, double tax)
        {
            var date = GetSafeDate(year, month, day);

            var taxModel = new Tax
            {
                Priority = TaxPriority.Daily,
                EndDate = date,
                StartDate = date,
                Value = tax
            };

            PostRequest<string>(uri, "api/tax/", taxModel, new { municipality = municipalityName });
        }

        private void AddWeeklyTax(Uri uri, string municipalityName, int year, int weekNumber, double value)
        {
            var startDate = FirstWeekDay(year, weekNumber).Date;
            var endDate = startDate.AddDays(7).Date;

            var taxModel = new Tax
            {
                Priority = TaxPriority.Weekly,
                EndDate = endDate,
                StartDate = startDate,
                Value = value
            };

            PostRequest<string>(uri, "api/tax/", taxModel, new { municipality = municipalityName });
        }

        private void AddMonthlyTax(Uri uri, string municipalityName, int year, int month, double tax)
        {
            var startDate = GetSafeDate(year, month, 1);
            var endDate = LastMonthDay(year, month).Date;

            var taxModel = new Tax
            {
                Priority = TaxPriority.Monthly,
                EndDate = endDate,
                StartDate = startDate,
                Value = tax
            };

            PostRequest<string>(uri, "api/tax/", taxModel, new { municipality = municipalityName });
        }

        private void AddYearlyTax(Uri uri, string municipalityName, int year, double tax)
        {
            var startDate = GetSafeDate(year, 1, 1);
            var endDate = GetSafeDate(year, 12, 31);

            var taxModel = new Tax
            {
                Priority = TaxPriority.Yearly,
                EndDate = endDate,
                StartDate = startDate,
                Value = tax
            };

            PostRequest<string>(uri, "api/tax/", taxModel, new { municipality = municipalityName });
        }

        private void PrintAllTaxes(Uri uri, string municipalityName)
        {
            var taxes = GetRequest<MunicipalityTaxes>(uri, "api/tax/", new { municipality = municipalityName });
            if (taxes.Taxes.Length < 1)
            {
                Console.WriteLine("No taxes for the municipality");
            }

            foreach (var tax in taxes.Taxes.OrderBy(x => x.Priority).ThenBy(x => x.StartDate))
            {
                Console.WriteLine($"{tax.Priority.ToString("G").PadRight(7)} {tax.StartDate:yyyy-MM-dd} {tax.EndDate:yyyy-MM-dd} {tax.Value:0.000}");
            }
        }

        private void PrintDayTax(Uri uri, string municipalityName, int year, int month, int day)
        {
            var tax = GetRequest<Tax>(uri, "api/tax/", new { municipality = municipalityName, year, month, day });
            Console.WriteLine($"{tax.Priority.ToString("G").PadRight(7)} {tax.StartDate:yyyy-MM-dd} {tax.EndDate:yyyy-MM-dd} {tax.Value:0.000}");
        }

        private static DateTime FirstWeekDay(int year, int weekOfYear)
        {
            if (weekOfYear < 1 || weekOfYear > 53)
            {
                throw new HaltException("Illegal week number");
            }

            var jan1 = GetSafeDate(year, 1, 1);
            var daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            var firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            var firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            var result = firstThursday.AddDays(weekNum * 7);

            return result.AddDays(-3);
        }

        private static DateTime LastMonthDay(int year, int month)
        {
            try
            {
                return GetSafeDate(year, month, DateTime.DaysInMonth(year, month));
            }
            catch (Exception ex)
            {
                throw new HaltException(ex.Message);
            }
        }

        private static DateTime GetSafeDate(int year, int month, int day)
        {
            try
            {
                return new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {
                throw new HaltException(ex.Message);
            }
        }
    }
}
