﻿using System.Collections.Generic;

namespace PZ.Tullia.BL.Shared.Services
{
    public interface IMunicipalityService
    {
        IEnumerable<string> GetAllMunicipalities();
        void AddMunicipality(string municipalityName);
        void ValidateMunicipalityName(string municipalityName);
    }
}
