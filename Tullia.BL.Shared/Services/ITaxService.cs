﻿using PZ.Tullia.BL.Shared.Models;

namespace PZ.Tullia.BL.Shared.Services
{
    public interface ITaxService
    {
        MunicipalityTaxes GetMunicipalityTaxes(string municipalityName);
        void AddMunicipalityTax(string municipality, Tax tax);
        void DeleteMunicipalityTax(string municipality, Tax tax);
        Tax GetMunicipalityDayTax(string municipalityName, int year, int month, int day);
    }
}
