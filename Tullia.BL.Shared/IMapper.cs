﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace PZ.Tullia.BL.Shared
{
    public interface IMapper<in TSource, TDestination>
    {
        TDestination Map(TSource source, TDestination destination = default(TDestination));

        [NotNull]
        IEnumerable<TDestination> Map([NotNull] IEnumerable<TSource> source);
    }
}
