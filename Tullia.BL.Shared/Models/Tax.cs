﻿using System;

namespace PZ.Tullia.BL.Shared.Models
{
    public class Tax
    {
        public TaxPriority Priority { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Value { get; set; }
    }
}
