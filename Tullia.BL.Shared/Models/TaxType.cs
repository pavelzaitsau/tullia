﻿namespace PZ.Tullia.BL.Shared.Models
{
    public enum TaxPriority
    {
        Yearly = 0,
        Monthly = 1,
        Weekly = 2,
        Daily = 3
    }
}
