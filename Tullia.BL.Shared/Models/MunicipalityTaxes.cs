﻿namespace PZ.Tullia.BL.Shared.Models
{
    public class MunicipalityTaxes
    {
        public string Municipality { get; set; }
        public Tax[] Taxes { get; set; }
    }
}
