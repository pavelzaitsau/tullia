﻿using System.Linq;
using FluentValidation;
using PZ.Tullia.BL.Shared.Exceptions;

namespace PZ.Tullia.BL.Shared
{
    public static class ExtensionsValidator
    {
        public static void ValidateWithBadRequest<T>(this IValidator<T> validator, T model, string message = null)
        {
            var result = validator.Validate(model);
            if (!result.IsValid)
            {
                throw new BadRequestException(message, result.Errors.ToArray());
            }
        }
    }
}
