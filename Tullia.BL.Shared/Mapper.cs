﻿using System.Collections.Generic;
using System.Linq;

namespace PZ.Tullia.BL.Shared
{
    public abstract class Mapper<TSource, TDestination> : IMapper<TSource, TDestination>
        where TDestination : class, new()
    {
        public IEnumerable<TDestination> Map(IEnumerable<TSource> source)
        {
            return source.Select(s => Map(s));
        }

        public TDestination Map(TSource source, TDestination destination = default(TDestination))
        {
            // ReSharper disable once CompareNonConstrainedGenericWithNull
#pragma warning disable RCS1165, RECS0017
            // Unconstrained type parameter checked for null.
            if (typeof(TSource).IsClass && source == null)
            {
                return null;
            }
#pragma warning restore RCS1165, RECS0017

            if (destination == null)
            {
                destination = new TDestination();
            }

            destination = InnerMap(source, destination);

            return destination;
        }

        protected abstract TDestination InnerMap(TSource source, TDestination destination);
    }
}
