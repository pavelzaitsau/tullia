﻿using System;
using FluentValidation;
using PZ.Tullia.BL.Shared.Models;

namespace PZ.Tullia.BL.Shared.Validators
{
    public class TaxValidator : BaseValidator<Tax>
    {
        public TaxValidator()
        {
            RuleFor(x => x.Priority).Must(IsDefinedEnumMember).WithMessage("Illegal tax type");
            RuleFor(x => x.StartDate).LessThanOrEqualTo(x => x.EndDate).WithMessage("Start date should be less or equal to end date");
            RuleFor(x => x.EndDate).LessThanOrEqualTo(DateTime.Now).WithMessage("End date should be less or equal than today");
            RuleFor(x => x.StartDate).GreaterThanOrEqualTo(new DateTime(1990, 1, 1)).WithMessage("Start date too far in the past");
        }

    }
}
