﻿using System;
using FluentValidation;

namespace PZ.Tullia.BL.Shared.Validators
{
    public abstract class BaseValidator<T> : AbstractValidator<T>
    {
        protected bool IsDefinedEnumMember<TE>(TE value)
        {
            return Enum.IsDefined(typeof(TE), value);
        }
    }
}
