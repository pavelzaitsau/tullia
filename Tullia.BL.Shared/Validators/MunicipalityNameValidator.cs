﻿using FluentValidation;

namespace PZ.Tullia.BL.Shared.Validators
{
    public class MunicipalityNameValidator : BaseValidator<string>
    {
        public MunicipalityNameValidator()
        {
            RuleFor(name => name).NotNull().NotEmpty().WithName("name");
            RuleFor(name => name).Length(3, 30).WithName("name");
        }
    }
}
