﻿using System.Net;

namespace PZ.Tullia.BL.Shared.Exceptions
{
    public class NotFoundException : PresentationException
    {
        public NotFoundException()
            : base(HttpStatusCode.NotFound)
        {
        }

        public NotFoundException(string message)
            : base(message, HttpStatusCode.NotFound)
        {
        }
    }
}
