﻿using System.Net;

namespace PZ.Tullia.BL.Shared.Exceptions
{
    public class ForbiddenException : PresentationException
    {
        public ForbiddenException()
            : base(HttpStatusCode.Forbidden)
        {
        }

        public ForbiddenException(string message)
            : base(message, HttpStatusCode.Forbidden)
        {
        }
    }
}
