﻿using System;
using System.Net;

namespace PZ.Tullia.BL.Shared.Exceptions
{
    public abstract class PresentationException : Exception
    {
        protected PresentationException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        protected PresentationException(string message, HttpStatusCode statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }

        public virtual ErrorModel ErrorModel => new ErrorModel(Message);
    }
}
