﻿using System.Linq;
using System.Net;
using FluentValidation.Results;
using JetBrains.Annotations;

namespace PZ.Tullia.BL.Shared.Exceptions
{
    public class BadRequestException : PresentationException
    {
        private readonly ErrorModel.FieldErrorModel[] _errors;

        public BadRequestException()
            : base(string.Empty, HttpStatusCode.BadRequest)
        {
        }

        public BadRequestException(string message)
            : base(message, HttpStatusCode.BadRequest)
        {
        }
        public BadRequestException(
            params ValidationFailure[] errors)
            : this(null, errors)
        {
        }

        public BadRequestException(
            [CanBeNull] string message,
            params ValidationFailure[] errors)
            : base(message, HttpStatusCode.BadRequest)
        {

            _errors = errors.Select(x => new ErrorModel.FieldErrorModel(x.PropertyName, x.ErrorMessage)).ToArray();
        }

        public override ErrorModel ErrorModel => new ErrorModel(Message, _errors);
    }
}
