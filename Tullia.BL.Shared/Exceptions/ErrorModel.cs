﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace PZ.Tullia.BL.Shared.Exceptions
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class ErrorModel
    {
        public ErrorModel()
            : this(new FieldErrorModel[0])
        {
        }

        public ErrorModel(
            [CanBeNull] string message)
            : this(message, new FieldErrorModel[0])
        {
        }

        public ErrorModel(
            [CanBeNull] string message,
            [NotNull] params KeyValuePair<string, string>[] errors)
            : this(message, errors.Select(x => new FieldErrorModel(x.Key, x.Value)).ToArray())
        {
        }

        public ErrorModel(
            [NotNull] params FieldErrorModel[] errors)
            : this(null, errors)
        {
        }

        public ErrorModel(
            [CanBeNull] string message,
            [NotNull] params FieldErrorModel[] errors)
        {
            Message = message;
            Errors = errors;
        }

        [UsedImplicitly]
        [JsonProperty("__message")]
        public string Message { get; }

        [UsedImplicitly]
        [JsonProperty("__errors")]
        public FieldErrorModel[] Errors { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        [Serializable]
        [JsonObject(MemberSerialization.OptIn)]
        public class FieldErrorModel
        {
            public FieldErrorModel(string field, string message)
            {
                Field = field;
                Message = message;
            }

            [UsedImplicitly]
            [JsonProperty("__field")]
            public string Field { get; }

            [UsedImplicitly]
            [JsonProperty("__message")]
            public string Message { get; }
        }
    }
}
