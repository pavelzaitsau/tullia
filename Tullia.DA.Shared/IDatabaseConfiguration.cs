﻿namespace PZ.Tullia.DA.Shared
{
    public interface IDatabaseConfiguration
    {
        string ConnectionString { get; }
        string SeedDatabase { get; }
    }
}
