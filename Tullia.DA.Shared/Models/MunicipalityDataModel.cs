﻿using JetBrains.Annotations;

namespace PZ.Tullia.DA.Shared.Models
{
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedWithFixedConstructorSignature, ImplicitUseTargetFlags.Members)]
    public class MunicipalityDataModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
