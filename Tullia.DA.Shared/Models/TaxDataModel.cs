﻿using System;
using JetBrains.Annotations;

namespace PZ.Tullia.DA.Shared.Models
{
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedWithFixedConstructorSignature, ImplicitUseTargetFlags.Members)]
    public class TaxDataModel
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Value { get; set; }
    }
}
