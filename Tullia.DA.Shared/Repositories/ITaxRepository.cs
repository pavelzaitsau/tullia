﻿using System;
using System.Collections.Generic;
using PZ.Tullia.DA.Shared.Models;

namespace PZ.Tullia.DA.Shared.Repositories
{
    public interface ITaxRepository
    {
        IEnumerable<TaxDataModel> GetTaxes(int municipalityId);
        IEnumerable<TaxDataModel> GetTaxesForInterval(int municipalityId, int priority, DateTime startDate, DateTime endDate);
        IEnumerable<TaxDataModel> GetTaxesForDate(int municipalityId, DateTime date);
        TaxDataModel GetTax(int municipalityId, int priority, DateTime startDate, DateTime endDate);
        void AddTax(int municipalityId, int priority, DateTime startDate, DateTime endDate, double value);
        void DeleteTax(int taxId);
    }
}
