﻿using System.Collections.Generic;
using PZ.Tullia.DA.Shared.Models;

namespace PZ.Tullia.DA.Shared.Repositories
{
    public interface IMunicipalityRepository
    {
        MunicipalityDataModel GetMunicipality(string municipalityName);
        IEnumerable<MunicipalityDataModel> GetMunicipalities();
        void AddMunicipality(string municipalityName);
    }
}
