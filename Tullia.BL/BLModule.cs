﻿using Autofac;
using PZ.Tullia.BL.Mapping;
using PZ.Tullia.BL.Shared.Services;
using PZ.Tullia.BL.Services;
using PZ.Tullia.BL.Shared;
using PZ.Tullia.BL.Shared.Models;
using PZ.Tullia.DA.Shared.Models;

namespace PZ.Tullia.BL
{
    public class BLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TaxService>().As<ITaxService>();
            builder.RegisterType<MunicipalityService>().As<IMunicipalityService>();

            builder.RegisterType<TaxMapper>().As<IMapper<TaxDataModel, Tax>>();
        }
    }
}
