﻿using System;
using System.Linq;
using FluentValidation;
using PZ.Tullia.BL.Shared;
using PZ.Tullia.BL.Shared.Exceptions;
using PZ.Tullia.BL.Shared.Models;
using PZ.Tullia.BL.Shared.Services;
using PZ.Tullia.BL.Shared.Validators;
using PZ.Tullia.DA.Shared.Models;
using PZ.Tullia.DA.Shared.Repositories;

namespace PZ.Tullia.BL.Services
{
    public class TaxService : ITaxService
    {
        private readonly ITaxRepository _taxRepository;
        private readonly IMunicipalityRepository _municipalityRepository;

        private readonly IMunicipalityService _municipalityService;

        private readonly IMapper<TaxDataModel, Tax> _mapper;

        private readonly IValidator<Tax> _taxValidator;

        public TaxService(
            ITaxRepository taxRepository,
            IMunicipalityRepository municipalityRepository,
            IMunicipalityService municipalityService,
            IMapper<TaxDataModel, Tax> mapper)
        {
            _taxRepository = taxRepository;
            _municipalityRepository = municipalityRepository;
            _municipalityService = municipalityService;
            _mapper = mapper;

            _taxValidator = new TaxValidator();
        }

        public MunicipalityTaxes GetMunicipalityTaxes(string municipalityName)
        {
            _municipalityService.ValidateMunicipalityName(municipalityName);

            var municipality = _municipalityRepository.GetMunicipality(municipalityName);
            if (municipality == null)
            {
                throw new NotFoundException("Municipality not exists");
            }

            var data = _taxRepository.GetTaxes(municipality.Id);
            var mapped = _mapper.Map(data).ToArray();

            return new MunicipalityTaxes
            {
                Municipality = municipalityName,
                Taxes = mapped
            };
        }

        public void AddMunicipalityTax(string municipalityName, Tax tax)
        {
            _municipalityService.ValidateMunicipalityName(municipalityName);

            ValidateTax(tax);

            var municipality = _municipalityRepository.GetMunicipality(municipalityName);
            if (municipality == null)
            {
                throw new NotFoundException("Municipality not exists");
            }

            var startDate = tax.StartDate.Date;
            var endDate = tax.EndDate.Date;

            var taxes =
                _taxRepository
                .GetTaxesForInterval(municipality.Id, (int)tax.Priority, startDate, endDate);

            if (taxes.Any())
            {
                throw new BadRequestException("The tax for some day from the interval already exists.");
            }

            _taxRepository.AddTax(municipality.Id, (int)tax.Priority, startDate, endDate, tax.Value);
        }

        public void DeleteMunicipalityTax(string municipalityName, Tax tax)
        {
            _municipalityService.ValidateMunicipalityName(municipalityName);
            ValidateTax(tax);

            var municipality = _municipalityRepository.GetMunicipality(municipalityName);
            if (municipality == null)
            {
                throw new NotFoundException("Municipality not exists.");
            }

            var data =
                _taxRepository
                .GetTax(municipality.Id, (int)tax.Priority, tax.StartDate.Date, tax.EndDate.Date);

            if (data == null)
            {
                throw new NotFoundException("Tax not found.");
            }

            _taxRepository.DeleteTax(data.Id);
        }

        public Tax GetMunicipalityDayTax(string municipalityName, int year, int month, int day)
        {
            _municipalityService.ValidateMunicipalityName(municipalityName);
            var date = GetValidTaxDate(year, month, day).Date;

            var municipality = _municipalityRepository.GetMunicipality(municipalityName);
            if (municipality == null)
            {
                throw new NotFoundException("Municipality not exists.");
            }

            var tax =
                _taxRepository
                .GetTaxesForDate(municipality.Id, date)
                .OrderByDescending(x => x.Priority)
                .FirstOrDefault();

            if (tax == null)
            {
                throw new NotFoundException("No taxes for the date.");
            }

            return _mapper.Map(tax);
        }

        private static DateTime GetValidTaxDate(int year, int month, int day)
        {
            try
            {
                return new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new BadRequestException("Year, Month, and Day describe an un-representable date");
            }
        }

        private void ValidateTax(Tax tax)
        {
            _taxValidator.ValidateWithBadRequest(tax);
        }
    }
}
