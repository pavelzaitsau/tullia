﻿using System.Collections.Generic;
using System.Linq;
using PZ.Tullia.BL.Shared;
using PZ.Tullia.BL.Shared.Exceptions;
using PZ.Tullia.BL.Shared.Services;
using PZ.Tullia.BL.Shared.Validators;
using PZ.Tullia.DA.Shared.Repositories;

namespace PZ.Tullia.BL.Services
{
    public class MunicipalityService : IMunicipalityService
    {
        private readonly IMunicipalityRepository _municipalityRepository;
        private readonly MunicipalityNameValidator _municipalityNameValidator;

        public MunicipalityService(IMunicipalityRepository municipalityRepository)
        {
            _municipalityRepository = municipalityRepository;
            _municipalityNameValidator = new MunicipalityNameValidator();
        }


        public IEnumerable<string> GetAllMunicipalities()
        {
            var data = _municipalityRepository.GetMunicipalities();
            return data.Select(x => x.Name);
        }

        public void AddMunicipality(string municipalityName)
        {
            ValidateMunicipalityName(municipalityName);

            var municipality = _municipalityRepository.GetMunicipality(municipalityName);
            if (municipality != null)
            {
                throw new BadRequestException("Municipality already exists");
            }

            _municipalityRepository.AddMunicipality(municipalityName);
        }

        public void ValidateMunicipalityName(string municipalityName)
        {
            if (string.IsNullOrWhiteSpace(municipalityName))
            {
                throw new BadRequestException("Municipality name can not be empty");
            }

            _municipalityNameValidator.ValidateWithBadRequest(municipalityName, "This municipality name is incorrect");
        }
    }
}
