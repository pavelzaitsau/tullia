﻿using PZ.Tullia.BL.Shared;
using PZ.Tullia.BL.Shared.Models;
using PZ.Tullia.DA.Shared.Models;

namespace PZ.Tullia.BL.Mapping
{
    public class TaxMapper : Mapper<TaxDataModel, Tax>
    {
        protected override Tax InnerMap(TaxDataModel source, Tax destination)
        {
            destination.Priority = (TaxPriority) source.Priority;
            destination.EndDate = source.EndDate;
            destination.StartDate = source.StartDate;
            destination.Value = source.Value;

            return destination;
        }
    }
}
