﻿using Autofac;
using PZ.Tullia.API.Configuration;
using PZ.Tullia.BL;
using PZ.Tullia.DA;
using PZ.Tullia.DA.Shared;

namespace PZ.Tullia.API
{
    internal class TulliaModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<BLModule>();
            builder.RegisterModule<DAModule>();

            builder.RegisterType<DatabaseConfiguration>().As<IDatabaseConfiguration>();
        }
    }
}
