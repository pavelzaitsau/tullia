﻿using Microsoft.Extensions.Configuration;
using PZ.Tullia.DA.Shared;

namespace PZ.Tullia.API.Configuration
{
    public class DatabaseConfiguration : IDatabaseConfiguration
    {
        public DatabaseConfiguration(
            IConfiguration configuration)
        {
            configuration.GetSection("Database").Bind(this);
        }

        public string ConnectionString { get; set; }
        public string SeedDatabase { get; set; }
    }
}
