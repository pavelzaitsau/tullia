﻿using Microsoft.AspNetCore.Mvc;
using PZ.Tullia.BL.Shared.Models;
using PZ.Tullia.BL.Shared.Services;

namespace PZ.Tullia.API.Controllers
{
    [Route("api/tax")]
    [ApiController]
    public class TaxController : ControllerBase
    {
        private readonly ITaxService _service;

        public TaxController(ITaxService service)
        {
            _service = service;
        }

        [Route("")]
        [HttpGet]
        public IActionResult GetMunicipalityTaxes([FromQuery]string municipality, [FromQuery]int year, [FromQuery]int month, [FromQuery]int day)
        {
            if (year > 0)
            {
                return Ok(_service.GetMunicipalityDayTax(municipality, year, month, day));
            }

            return Ok(_service.GetMunicipalityTaxes(municipality));
        }

        [Route("")]
        [HttpPost]
        public void AddMunicipalityTax([FromQuery] string municipality, [FromBody]Tax tax)
        {
            _service.AddMunicipalityTax(municipality, tax);
        }
    }
}
