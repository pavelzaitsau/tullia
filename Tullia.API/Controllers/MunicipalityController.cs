﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PZ.Tullia.BL.Shared.Services;

namespace PZ.Tullia.API.Controllers
{
    [Route("api/municipality")]
    [ApiController]
    public class MunicipalityController : ControllerBase
    {
        private readonly IMunicipalityService _service;

        public MunicipalityController(IMunicipalityService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<string> GetAllMunicipalities()
        {
            return _service.GetAllMunicipalities();
        }

        [HttpPost]
        public void AddMunicipality([FromQuery] string municipalityName)
        {
            _service.AddMunicipality(municipalityName);
        }
    }
}
