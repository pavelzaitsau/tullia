﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NLog;
using PZ.Tullia.BL.Shared.Exceptions;

namespace PZ.Tullia.API.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private static Logger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex).ConfigureAwait(false);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;
            ErrorModel errorModel;

            switch (exception)
            {
                case PresentationException presentationException:
                {
                    code = presentationException.StatusCode;
                    errorModel = presentationException.ErrorModel;
                    break;
                }
                default:
                {
                    _logger.Error(exception);
                    code = HttpStatusCode.InternalServerError;
                    errorModel = new ErrorModel("Oops. Something went wrong");
                    break;
                }
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(errorModel.ToString());
        }
    }
}
